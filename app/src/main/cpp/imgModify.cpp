/*************************************************************
*< 	FILE			imgModify.cpp

	DESCRIPTION		modify rgb array ( 3 bytes for pixel)
	
	HISTORY			01.10.2017

*************************************************************/
#include <jni.h>
#include <string>
#include "imgModify.h"


bool initImage2dArray(char *input_img, int height, int width, int channel) {
    unsigned int size = RGB_size * sizeof(char) * height * width;

    m_image.data = input_img;
    m_image.height = height;
    m_image.width = width;
    m_image.channel = channel;
    m_image.size = size;
    modifyPixel(10, 10, (char) 255, (char) 0, (char) 0);
    return true;

}

bool modifyPixel(int x, int y, char red, char blue, char green) {
    if (!m_image.data) return false;
    unsigned int idx = RGB_size * m_image.width * y + RGB_size * x;
    if (idx < m_image.size)
        m_image.data[idx] = red;
    if (idx + 1 < m_image.size)
        m_image.data[idx + 1] = blue;
    if (idx + 2 < m_image.size)
        m_image.data[idx + 2] = green;
    return true;
}


int getWidth() {
    return m_image.width;
}

int getHeight() {
    return m_image.height;
}

char *getData() {
    return m_image.data;
}
