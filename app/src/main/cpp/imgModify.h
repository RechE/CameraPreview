/*************************************************************
*< 	FILE			imgModify.h

	DESCRIPTION		modify rgb array ( 3 bytes for pixel)
	
	HISTORY			01.10.2017

*************************************************************/

#ifndef _MODIFY_H_
#define _MODIFY_H_

const int RGB_size = 3;
const int RGBA_size = 4;

typedef struct
{
	char* data;
	int channel;
	int width;
	int height;
	unsigned int size;
} IMAGE;


static IMAGE m_image;

bool initImage2dArray(char* input_img, int height, int width, int channel);

bool modifyPixel(int x, int y, char red, char blue, char green);

int getWidth();

int getHeight();

char* getData();

#endif