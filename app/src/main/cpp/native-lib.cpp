#include <jni.h>
#include <string>
#include "imgModify.h"

void throwJavaException(JNIEnv *env, const char *msg)
{
    jclass c = env->FindClass("java/lang/RuntimeException");
    if (NULL == c)
    {
        c = env->FindClass("java/lang/NullPointerException");
    }
    env->ThrowNew(c, msg);
}

extern "C"
JNIEXPORT jboolean JNICALL
Java_com_example_camera_v1_CameraActivityApi1_modifyPixelJNI(JNIEnv *env, jobject instance, jint x,
                                                             jint y, jchar red, jchar blue,
                                                             jchar green) {
    jboolean isModified;
    try
    {
        isModified =(jboolean) modifyPixel(x, y, (char) red, (char) blue, (char) green);
    }
        // You can catch std::exception for more generic error handling
    catch ( std::exception exception)
    {
        throwJavaException (env, exception.what());
    }
    return isModified;
}

extern "C"
JNIEXPORT jboolean JNICALL
Java_com_example_camera_v1_CameraActivityApi1_initImage2dArrayJNI(JNIEnv *env, jobject instance,
                                                                  jchar input_img, jint height,
                                                                  jint width, jint channel) {

    return (jboolean) initImage2dArray((char *) input_img, height, width, channel);
}



extern "C"
JNIEXPORT jstring JNICALL
Java_com_example_camera_v2_CameraActivityApi2_stringFromJNI(JNIEnv *env, jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
extern "C"
JNIEXPORT jstring JNICALL
Java_com_example_camera_v1_CameraActivityApi1_stringFromJNI(JNIEnv *env, jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

