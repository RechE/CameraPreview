package com.example.camera.v1;

import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Handler;
import android.renderscript.RenderScript;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.View;

import com.example.camera.R;
import com.example.camera.databinding.ActivityCameraApi1Binding;

import java.io.IOException;
import java.util.List;


public class CameraActivityApi1 extends AppCompatActivity implements SurfaceHolder.Callback, Camera.PreviewCallback {


    private static final String TAG = "CameraActivityApi1";
    ActivityCameraApi1Binding binding;
    private Camera camera;
    private SurfaceTexture surfaceTexture;
    private RenderScript mRs;
    private int mBufferSize;
    private SurfaceHolder surfaceHolder;
    private boolean isRGBOn = false;
    private int mFPSCounter = 0;

    static {
        System.loadLibrary("native-lib");
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_camera_api_1);
        mRs = RenderScript.create(this);
        startFPSTracking();
        setOnListeners();
    }

    private void setOnListeners() {
        binding.mode.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            isRGBOn = isChecked;
            binding.rgbPreview.setVisibility(isRGBOn ? View.VISIBLE : View.GONE);
            binding.modeType.setText(isRGBOn ? getString(R.string.mode_rgb_on)
                    : getString(R.string.mode_rgb_off));
            resetCamera();
            setCamera();
        });
    }

    private void setCamera() {
        camera = Camera.open();
        Camera.Parameters params = camera.getParameters();
        chooseOptimalSize(params.getSupportedPictureSizes());
        params.setPreviewSize(640, 480);
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        camera.setParameters(params);
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_BACK, info);
        camera.setDisplayOrientation(getCorrectCameraOrientation(info, camera));
        binding.surfacePreview.getHolder().addCallback(this);
        binding.surfacePreview.setVisibility(View.VISIBLE);
    }

    private void chooseOptimalSize(List<Size> sizes) {
        for (Size size : sizes) {
            Log.d(TAG, "chooseOptimalSize: width: " + size.width + "height: " + size.height);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (camera == null) {
            setCamera();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        resetCamera();
    }

    private void resetCamera() {
        binding.surfacePreview.setVisibility(View.GONE);
        camera.stopPreview();
        camera.setPreviewCallback(null);
        camera.setPreviewCallbackWithBuffer(null);
        camera.release();
        camera = null;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        startCameraPreview(holder);
    }

    public void startCameraPreview(SurfaceHolder holder) {
        this.surfaceHolder = holder;
        try {
            if (camera != null) {
                Camera.Parameters parameters = camera.getParameters();
                Size previewSize = parameters.getPreviewSize();
                int imageFormat = parameters.getPreviewFormat();
                if (imageFormat != ImageFormat.NV21) {
                    throw new UnsupportedOperationException();
                }
                mBufferSize = previewSize.width * previewSize.height * ImageFormat.getBitsPerPixel(imageFormat) / 8;
                byte[] mCallbackBuffer = new byte[mBufferSize];
                camera.setPreviewCallbackWithBuffer(this);
                camera.addCallbackBuffer(mCallbackBuffer);
                if (isRGBOn) {
                    Log.d(TAG, "startCameraPreview: TRUE");
                    camera.setPreviewTexture(new SurfaceTexture(10));
                } else {
                    Log.d(TAG, "startCameraPreview: TRUE");
                    camera.setPreviewDisplay(holder);
                }
                camera.startPreview();

            }

        } catch (IOException e) {
            Log.d(TAG, "Error setting camera preview: " + e.getMessage());
        }
    }

    @Override
    public void onPreviewFrame(byte[] yuvFrame, Camera camera) {
        if (yuvFrame == null) {
            return;
        }
        int expectedBytes = mBufferSize;
        if (expectedBytes != yuvFrame.length) {
            Log.e(TAG, "Mismatched size of buffer!  ");
            return;
        }

        try {
            Log.d(TAG, "onPreviewFrame: Frame" + getFPSCounter());
            setFPSCounter(mFPSCounter + 1);
            if (isRGBOn) {
                Camera.Parameters parameters = this.camera.getParameters();
                Size imageSize = parameters.getPreviewSize();
                Bitmap rgbBitmap = RenderScriptHelper.convertYuvToRgbIntrinsic(mRs, yuvFrame, imageSize.width, imageSize.height);
                drawRectangleOnBitmap(rgbBitmap, rgbBitmap.getWidth() / 2, rgbBitmap.getHeight() / 2);
                binding.rgbPreview.setImageBitmap(rgbBitmap);
            }
        } catch (Exception e) {
            Log.d(TAG, "onPreviewFrame: Exception: " + e.toString());
        }

        camera.addCallbackBuffer(yuvFrame);

    }

    private void startFPSTracking() {
        Handler handler = new Handler();
        int delay = 1000; //milliseconds

        handler.postDelayed(new Runnable() {
            public void run() {
                String fps = "FPS: " + getFPSCounter();
                runOnUiThread(() -> {
                    Log.d(TAG, "run: fps GET FPS COUNTER" + getFPSCounter());
                    binding.txtFps.setText(fps);
                });
                setFPSCounter(0);
                handler.postDelayed(this, delay);
            }
        }, delay);
    }

    synchronized private void setFPSCounter(int num) {
        mFPSCounter = num;
    }

    synchronized private int getFPSCounter() {
        return mFPSCounter;
    }

    public int getCorrectCameraOrientation(Camera.CameraInfo info, Camera camera) {

        int rotation = this.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;

            case Surface.ROTATION_90:
                degrees = 90;
                break;

            case Surface.ROTATION_180:
                degrees = 180;
                break;

            case Surface.ROTATION_270:
                degrees = 270;
                break;

        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;
        } else {
            result = (info.orientation - degrees + 360) % 360;
        }

        return result;
    }

    private void drawRectangleOnBitmap(Bitmap bitmap, int x, int y) {
        int x1 = x + 10;
        int y1 = y + 10;
        for (int i = x; i <= x1; i++) {
            for (int k = y; k <= y1; k++) {
                bitmap.setPixel(i, k, Color.rgb(255, 0, 0));
            }
        }
    }


    public void surfaceDestroyed(SurfaceHolder holder) {

    }


    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {

    }

    public native boolean initImage2dArrayJNI(char input_img, int height, int width, int channel);

    public native String stringFromJNI();

    public native boolean modifyPixelJNI(int x, int y, char red, char blue, char green);
}
//         if (initImage2dArrayJNI((char) rgbBitmap.getRowBytes(), rgbBitmap.getWidth(), rgbBitmap.getHeight(), 255)) {
//                Log.d(TAG, "onPreviewFrame: Success ");
//            }
//            if (modifyPixelJNI(rgbBitmap.getWidth() / 2, rgbBitmap.getHeight() / 2, (char) 255, (char) 0, (char) 0)) {
//                Log.d(TAG, "onPreviewFrame: Success pixel modifying");
//            }
