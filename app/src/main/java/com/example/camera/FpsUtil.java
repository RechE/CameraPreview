package com.example.camera;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import static android.content.ContentValues.TAG;



public class FpsUtil {

    private static int fpsCounter;

    public static void startFPSTracking(Activity activity, TextView textView) {
        Handler handler = new Handler();
        int delay = 1000; //milliseconds

        handler.postDelayed(new Runnable() {
            public void run() {
                String fps = "FPS: " + getFPSCounter();
                activity.runOnUiThread(() -> {
                    Log.d(TAG, "run: fps GET FPS COUNTER" + getFPSCounter());
                    textView.setText(fps);
                });
                setFPSCounter(0);
                handler.postDelayed(this, delay);
            }
        }, delay);
    }

    synchronized static public void setFPSCounter(int num) {
        fpsCounter = num;
    }

    synchronized static public int getFPSCounter() {
        return fpsCounter;
    }
}
