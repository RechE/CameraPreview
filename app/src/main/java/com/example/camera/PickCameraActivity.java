package com.example.camera;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.camera.v1.CameraActivityApi1;
import com.example.camera.v2.CameraActivityApi2;
import com.example.camera.v2.CameraActivityApi2New;

import static android.R.attr.start;

public class PickCameraActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_camera);
    }

    public void cameraV1Clicked(View view) {
        startActivity(new Intent(this, CameraActivityApi1.class));
    }

    public void cameraV2Clicked(View view) {
        startActivity(new Intent(this, CameraActivityApi2New.class));
    }
}
