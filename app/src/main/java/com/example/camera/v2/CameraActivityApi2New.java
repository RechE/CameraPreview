package com.example.camera.v2;

import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.renderscript.RenderScript;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.tipit.Tipit;
import com.example.camera.R;
import com.example.camera.databinding.ActivityCameraApi2Binding;
import com.example.camera.v1.RenderScriptHelper;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import io.fotoapparat.Fotoapparat;
import io.fotoapparat.parameter.LensPosition;
import io.fotoapparat.parameter.ScaleType;
import io.fotoapparat.parameter.Size;
import io.fotoapparat.preview.Frame;
import io.fotoapparat.preview.FrameProcessor;

import static io.fotoapparat.parameter.selector.FocusModeSelectors.autoFocus;
import static io.fotoapparat.parameter.selector.FocusModeSelectors.continuousFocus;
import static io.fotoapparat.parameter.selector.FocusModeSelectors.fixed;
import static io.fotoapparat.parameter.selector.LensPositionSelectors.lensPosition;
import static io.fotoapparat.parameter.selector.Selectors.firstAvailable;

public class CameraActivityApi2New extends AppCompatActivity {

    ActivityCameraApi2Binding binding;
    public static final String TAG = CameraActivityApi2New.class.getSimpleName();
    private static final String MODEL_FILE = "file:///android_asset/constant_graph_weights.pb";
    private static final String INPUT_NODE = "data_1:0";
    private static final String OUTPUT_NODE = "up_tiny2vga_1/conv2d_transpose:0";
    private static final int WIDTH = 480;
    private static final int HEIGHT = 640;
    private Fotoapparat backFotoapparat;
    private RenderScript mRs;
    private TensorFlowInferenceInterface tensorflow;
    private int[] intValues = new int[WIDTH * HEIGHT];
    private float[] floatValues = new float[WIDTH * HEIGHT * 3];
    private int fpsCounter = 0;
    private boolean isFirstLaunch;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_camera_api_2);
        backFotoapparat = createFotoapparat(LensPosition.BACK);
        mRs = RenderScript.create(this);
        tensorflow = new TensorFlowInferenceInterface(getAssets(), MODEL_FILE);
        startFPSTracking();
        setListeners();
    }

    private void setListeners() {
        binding.mode.setOnCheckedChangeListener((compoundButton, isChecked) -> {
            binding.rgbPreview.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            binding.modeType.setText(isChecked ? getString(R.string.mode_rgb_on)
                    : getString(R.string.mode_rgb_off));
        });
    }

    private Fotoapparat createFotoapparat(LensPosition position) {
        return Fotoapparat
                .with(this)
                .into(binding.cameraView)
                .previewScaleType(ScaleType.CENTER_CROP)
                .previewSize(items -> new Size(WIDTH, HEIGHT))
                .lensPosition(lensPosition(position))
                .focusMode(firstAvailable(
                        continuousFocus(),
                        autoFocus(),
                        fixed()
                ))
                .frameProcessor(new SampleFrameProcessor())
                .cameraErrorCallback(e ->
                        Toast.makeText(CameraActivityApi2New.this, e.toString(), Toast.LENGTH_LONG).show())
                .build();
    }

    public void processImageTensorflowly(View view) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap realImage = BitmapFactory.decodeResource(getResources(), R.drawable.man_picture, options);
        Log.d(TAG, "processImageTensorflowly: real Image size: " + realImage.getWidth() + " : " + realImage.getHeight());
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(realImage, HEIGHT,
                WIDTH, false);
        scaledBitmap.getPixels(intValues, 0, scaledBitmap.getWidth(), 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight());
        Log.d(TAG, "processImageTensorflowly: scaledBitmap LENGTH: " + intValues.length);
        Log.d(TAG, "processImageTensorflowly: real image after scale" + scaledBitmap.getWidth() + " : " + scaledBitmap.getHeight());
        processImageWithTensorFlow(scaledBitmap);
        binding.rgbPreview.setImageBitmap(scaledBitmap);
    }

    private class SampleFrameProcessor implements FrameProcessor {

        @Override
        public void processFrame(Frame frame) {
            if (isFirstLaunch) return;
            Log.d(TAG, "processFrame: Frame");
            setFPSCounter(fpsCounter + 1);
            Bitmap rgbBitmap = RenderScriptHelper.convertYuvToRgbIntrinsic(mRs, frame.image, WIDTH, HEIGHT);
            Tipit.drawRectangleOnBitmap(rgbBitmap, rgbBitmap.getWidth() / 2, rgbBitmap.getHeight() / 2);
            Log.d(TAG, "processFrame: rgbBitmap before TensorFlow: " + rgbBitmap.getWidth() + " : " + rgbBitmap.getHeight());
//            processImageWithTensorFlow(rgbBitmap);
            Log.d(TAG, "processFrame: rgbBitmap after TensorFlow: " + rgbBitmap.getWidth() + " : " + rgbBitmap.getHeight());
            runOnUiThread(() ->
                    binding.rgbPreview.setImageBitmap(rgbBitmap)
            );
//            isFirstLaunch = true;
            // Perform frame processing, if needed
        }
    }

    private void startFPSTracking() {
        Handler handler = new Handler();
        int delay = 1000; //milliseconds

        handler.postDelayed(new Runnable() {
            public void run() {
                String fps = "FPS: " + getFPSCounter();
                runOnUiThread(() -> {
                    Log.d(TAG, "run: fps GET FPS COUNTER" + getFPSCounter());
                    binding.txtFps.setText(fps);
                });
                setFPSCounter(0);
                handler.postDelayed(this, delay);
            }
        }, delay);
    }

    private void processImageWithTensorFlow(final Bitmap bitmap) {
        Log.d(TAG, "processImageWithTensorFlow: Image processed start");
        bitmap.getPixels(intValues, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        for (int i = 0; i < 10; i++) {
            Log.d(TAG, "processImageWithTensorFlow: PIXELS: " + intValues[i]);


            Log.d(TAG, "processImageWithTensorFlow: PIXELS: " + intValues[i]);
        }

        Log.d(TAG, "processImageWithTensorFlow: red" + Color.red(intValues[0]));
        Log.d(TAG, "processImageWithTensorFlow: green" + Color.green(intValues[0]));
        Log.d(TAG, "processImageWithTensorFlow: blue" + Color.blue(intValues[0]));

        for (int i = 0; i < intValues.length; ++i) {
            final int val = intValues[i];
            floatValues[i * 3] = ((val >> 16) & 0xFF) / 255.0f;
            floatValues[i * 3 + 1] = ((val >> 8) & 0xFF) / 255.0f;
            floatValues[i * 3 + 2] = (val & 0xFF) / 255.0f;
        }

        Log.d(TAG, "processImageWithTensorFlow: FLOAT INPUT VALUES LENGTH" + floatValues.length);
        Log.d(TAG, "processImageWithTensorFlow: FLOAT INPUT VALUES LENGTH" + floatValues[0]);
//
        tensorflow.feed(
                INPUT_NODE, floatValues, 1, bitmap.getWidth(), bitmap.getHeight(), 3);

        tensorflow.run(new String[]{OUTPUT_NODE}, false);
        tensorflow.fetch(OUTPUT_NODE, floatValues);
        Log.d(TAG, "processImageWithTensorFlow: FLOAT OUTPUT VALUES LENGTH" + floatValues.length);
        Log.d(TAG, "processImageWithTensorFlow: FLOAT OUTPUT VALUES LENGTH" + floatValues[0]);
        for (int i = 0; i < 10; i++) {
            Log.d(TAG, "processImageWithTensorFlow: PIXELS: " + intValues[i]);
        }
        for (int i = 0; i < intValues.length; ++i) {
            intValues[i] =
                    0xFF000000
                            | (((int) (floatValues[i * 3] * 255)) << 16)
                            | (((int) (floatValues[i * 3 + 1] * 255)) << 8)
                            | ((int) (floatValues[i * 3 + 2] * 255));
        }

        Log.d(TAG, "processImageWithTensorFlow: red" + Color.red(intValues[0]));
        Log.d(TAG, "processImageWithTensorFlow: green" + Color.green(intValues[0]));
        Log.d(TAG, "processImageWithTensorFlow: blue" + Color.blue(intValues[0]));

        bitmap.setPixels(intValues, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        Log.d(TAG, "processImageWithTensorFlow: Image processed successfully");
    }

    @Override
    protected void onStart() {
        super.onStart();
//        backFotoapparat.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        backFotoapparat.stop();
    }

    synchronized private void setFPSCounter(int num) {
        fpsCounter = num;
    }

    synchronized private int getFPSCounter() {
        return fpsCounter;
    }


}
