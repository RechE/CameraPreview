package com.example.camera.v2;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicYuvToRGB;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Range;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;
import android.widget.FrameLayout;

import com.example.camera.R;
import com.example.camera.databinding.ActivityCameraApi2Binding;
import com.example.camera.v1.RenderScriptHelper;

import java.nio.ByteBuffer;
import java.util.Arrays;

import static android.graphics.PixelFormat.RGBA_8888;

public class CameraActivityApi2 extends AppCompatActivity {

    public static final String TAG = CameraActivityApi2.class.getSimpleName();
    public static final int CAMERA_REQUEST_CODE = 1001;
    ActivityCameraApi2Binding binding;
    CameraManager cameraManager;
    TextureView.SurfaceTextureListener surfaceTextureListener;
    HandlerThread backgroundThread;
    Handler backgroundHandler;
    CameraDevice cameraDevice;
    Size previewSize;
    String cameraId;
    CameraCaptureSession cameraCaptureSession;
    ImageReader imageReader;
    ImageReader.OnImageAvailableListener onImageAvailableListener;
    int cameraFacing;
    RenderScript rs;
    ScriptIntrinsicYuvToRGB yuvToRgbIntrinsic;
    int yuvDatalength;
    private boolean isRGBOn = false;
    private int mFPSCounter = 0;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_camera_api_2);
        cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        cameraFacing = CameraCharacteristics.LENS_FACING_BACK;
        startFPSTracking();
        surfaceTextureListener = new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
                setUpCamera();
                openCamera();
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

            }
        };
    }

    private void createRenderScript() {
        rs = RenderScript.create(this);
        yuvToRgbIntrinsic = ScriptIntrinsicYuvToRGB.create(rs, Element.U8_4(rs));
        yuvDatalength = previewSize.getWidth() * previewSize.getHeight() * 3 / 2;  // this is 12 bit per pixel
    }

    private void setUpCamera() {
        try {
            for (String cameraId : cameraManager.getCameraIdList()) {
                CameraCharacteristics cameraCharacteristics =
                        cameraManager.getCameraCharacteristics(cameraId);
                if (cameraCharacteristics.get(CameraCharacteristics.LENS_FACING) ==
                        cameraFacing) {
                    StreamConfigurationMap streamConfigurationMap = cameraCharacteristics.get(
                            CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                    int[] formats = streamConfigurationMap.getOutputFormats();
                    for (int i = 0; i < formats.length; i++) {
                        Log.d(TAG, "setUpCamera: FORMATS: " + formats[i]);
                    }
                    Range<Integer>[] range = streamConfigurationMap.getHighSpeedVideoFpsRanges();
                    for (int i = 0; i < range.length; i++) {
                        Log.d(TAG, "setUpCamera: RANGE FPS" + range[i]);
                    }
                    Log.d(TAG, "setUpCamera: RANGE SIZE: " + range.length);
                    previewSize = chooseOptimalSize(streamConfigurationMap.getOutputSizes(SurfaceTexture.class), 1024, 720);
                    this.cameraId = cameraId;
                    initReader();
                    createRenderScript();

                }
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void startFPSTracking() {
        Handler handler = new Handler();
        int delay = 1000; //milliseconds

        handler.postDelayed(new Runnable() {
            public void run() {
                String fps = "FPS: " + getFPSCounter();
                runOnUiThread(() -> {
                    Log.d(TAG, "run: fps GET FPS COUNTER" + getFPSCounter());
                    binding.txtFps.setText(fps);
                });
                setFPSCounter(0);
                handler.postDelayed(this, delay);
            }
        }, delay);
    }

    synchronized private void setFPSCounter(int num) {
        mFPSCounter = num;
    }

    synchronized private int getFPSCounter() {
        return mFPSCounter;
    }


    private void initReader() {
        onImageAvailableListener = reader -> {
            setFPSCounter(mFPSCounter + 1);
            Image image = reader.acquireLatestImage();
            Log.d(TAG, "initReader: Frame");
//            Bitmap bitmap = RenderScriptHelper.convertYuvToRgbIntrinsic(rs, Utils2.YUV_420_888toNV21(image),
//                    previewSize.getWidth(), previewSize.getHeight());
//            Bitmap bitmap = Utils.convertToBitmap(image);
//            runOnUiThread(() ->
//                    binding.previewImage.setImageBitmap(bitmap));
            Log.d(TAG, "onPreviewFrame: Frame" + getFPSCounter());
            image.close();
        };
        Log.d(TAG, "initReader: width: " + previewSize.getWidth() + " height: " + previewSize.getHeight());
        imageReader = ImageReader.newInstance(640, 480,
                ImageFormat.YV12, 5);
        imageReader.setOnImageAvailableListener(
                onImageAvailableListener, backgroundHandler);
    }

    private Bitmap convertImageToBitmap(Image image) {
        Bitmap bitmap = Bitmap.createBitmap(previewSize.getWidth(), previewSize.getHeight(), Bitmap.Config.ARGB_8888);
        Allocation yuvPreviewAlloc = Allocation.createSized(rs, Element.U8(rs), yuvDatalength);

        Allocation rgbOutputAlloc = Allocation.createFromBitmap(rs, bitmap);  // this simple !

        yuvToRgbIntrinsic.setInput(yuvPreviewAlloc);
        yuvPreviewAlloc.copyFrom(Utils2.YUV420toNV21(image));  // or yuvPreviewAlloc.copyFromUnchecked(data);

        yuvToRgbIntrinsic.forEach(rgbOutputAlloc);

        rgbOutputAlloc.copyTo(bitmap);
        return bitmap;
    }

    private Bitmap imageToBitmap(Image image) {
        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
        byte[] bytes = new byte[buffer.remaining()];
        buffer.get(bytes);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length, null);
    }

    private void openCamera() {
        try {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED) {
                cameraManager.openCamera(cameraId, stateCallback, backgroundHandler);
            }
        } catch (CameraAccessException e) {
            Log.d(TAG, "openCamera: " + e.toString());
            e.printStackTrace();
        }
    }

    private void openBackgroundThread() {
        backgroundThread = new HandlerThread("camera_background_thread");
        backgroundThread.start();
        backgroundHandler = new Handler(backgroundThread.getLooper());
    }

    CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice cameraDevice) {
            CameraActivityApi2.this.cameraDevice = cameraDevice;
            createPreviewSession();
        }

        @Override
        public void onDisconnected(CameraDevice cameraDevice) {
            cameraDevice.close();
            CameraActivityApi2.this.cameraDevice = null;
        }

        @Override
        public void onError(CameraDevice cameraDevice, int error) {
            cameraDevice.close();
            CameraActivityApi2.this.cameraDevice = null;
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        openBackgroundThread();
        if (binding.textureView.isAvailable()) {
            setUpCamera();
            openCamera();
        } else {
            binding.textureView.setSurfaceTextureListener(surfaceTextureListener);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        closeCamera();
        closeBackgroundThread();
    }

    private void closeCamera() {
        if (cameraCaptureSession != null) {
            cameraCaptureSession.close();
            cameraCaptureSession = null;
        }

        if (cameraDevice != null) {
            cameraDevice.close();
            cameraDevice = null;
        }
    }

    private void createPreviewSession() {
        try {
            SurfaceTexture surfaceTexture = binding.textureView.getSurfaceTexture();
            surfaceTexture.setDefaultBufferSize(640, 480);
            Surface previewSurface = new Surface(surfaceTexture);
            CaptureRequest.Builder captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(previewSurface);
            captureRequestBuilder.addTarget(imageReader.getSurface());
            //Collections.singletonList(previewSurface)
            //Arrays.asList(previewSurface, imageReader.getSurface())
            cameraDevice.createCaptureSession(Arrays.asList(previewSurface, imageReader.getSurface()),
                    new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(CameraCaptureSession cameraCaptureSession) {
                            if (cameraDevice == null) {
                                return;
                            }

                            try {
                                CaptureRequest captureRequest = captureRequestBuilder.build();
                                CameraActivityApi2.this.cameraCaptureSession = cameraCaptureSession;
                                CameraActivityApi2.this.cameraCaptureSession.setRepeatingRequest(captureRequest,
                                        null, backgroundHandler);
                            } catch (CameraAccessException e) {
                                Log.d(TAG, "onConfigured: " + e.toString());
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {

                        }
                    }, backgroundHandler);
        } catch (Exception e) {
            Log.d(TAG, "createPreviewSession: " + e.toString());
            e.printStackTrace();
        }
    }

    private Size chooseOptimalSize(Size[] outputSizes, int width, int height) {
        double preferredRatio = height / (double) width;
        Size currentOptimalSize = outputSizes[0];
        double currentOptimalRatio = currentOptimalSize.getWidth() / (double) currentOptimalSize.getHeight();
        for (Size currentSize : outputSizes) {
            Log.d(TAG, "chooseOptimalSize: width: " + currentSize.getWidth() + "height: " + currentSize.getHeight());
            double currentRatio = currentSize.getWidth() / (double) currentSize.getHeight();
            if (Math.abs(preferredRatio - currentRatio) <
                    Math.abs(preferredRatio - currentOptimalRatio)) {
                currentOptimalSize = currentSize;
                currentOptimalRatio = currentRatio;
            }
        }
        return currentOptimalSize;
    }

    private void setAspectRatioTextureView() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;
        Log.d(TAG, "setAspectRatioTextureView: width: " + width + "height: " + height);
        if (width > height) {
            int newWidth = width;
            int newHeight = ((width * width) / height);
            updateTextureViewSize(newWidth, newHeight);

        } else {
            int newWidth = width;
            int newHeight = ((width * height) / width);
            updateTextureViewSize(newWidth, newHeight);
        }

    }

    private void updateTextureViewSize(int viewWidth, int viewHeight) {
        Log.d(TAG, "TextureView Width : " + viewWidth + " TextureView Height : " + viewHeight);
        binding.textureView.setLayoutParams(new FrameLayout.LayoutParams(viewWidth, viewHeight));
    }

    private void closeBackgroundThread() {
        if (backgroundHandler != null) {
            backgroundThread.quitSafely();
            backgroundThread = null;
            backgroundHandler = null;
        }
    }

    static {
        System.loadLibrary("native-lib");

    }

    public native String stringFromJNI();

}
