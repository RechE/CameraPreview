package com.example.camera.v2;

import android.databinding.BindingMethod;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.media.Image;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;

import static java.lang.System.out;


public class Utils {

    private void getRGBIntFromPlanes(Image.Plane[] planes, int[] mRgbBuffer, int mHeight) {
        ByteBuffer yPlane = planes[0].getBuffer();
        ByteBuffer uPlane = planes[1].getBuffer();
        ByteBuffer vPlane = planes[2].getBuffer();

        int bufferIndex = 0;
        final int total = yPlane.capacity();
        final int uvCapacity = uPlane.capacity();
        final int width = planes[0].getRowStride();

        int yPos = 0;
        for (int i = 0; i < mHeight; i++) {
            int uvPos = (i >> 1) * width;

            for (int j = 0; j < width; j++) {
                if (uvPos >= uvCapacity - 1)
                    break;
                if (yPos >= total)
                    break;

                final int y1 = yPlane.get(yPos++) & 0xff;

            /*
              The ordering of the u (Cb) and v (Cr) bytes inside the planes is a
              bit strange. The _first_ byte of the u-plane and the _second_ byte
              of the v-plane build the u/v pair and belong to the first two pixels
              (y-bytes), thus usual YUV 420 behavior. What the Android devs did
              here (IMHO): just copy the interleaved NV21 U/V data to two planes
              but keep the offset of the interleaving.
             */
                final int u = (uPlane.get(uvPos) & 0xff) - 128;
                final int v = (vPlane.get(uvPos + 1) & 0xff) - 128;
                if ((j & 1) == 1) {
                    uvPos += 2;
                }

                // This is the integer variant to convert YCbCr to RGB, NTSC values.
                // formulae found at
                // https://software.intel.com/en-us/android/articles/trusted-tools-in-the-new-android-world-optimization-techniques-from-intel-sse-intrinsics-to
                // and on StackOverflow etc.
                final int y1192 = 1192 * y1;
                int r = (y1192 + 1634 * v);
                int g = (y1192 - 833 * v - 400 * u);
                int b = (y1192 + 2066 * u);

                r = (r < 0) ? 0 : ((r > 262143) ? 262143 : r);
                g = (g < 0) ? 0 : ((g > 262143) ? 262143 : g);
                b = (b < 0) ? 0 : ((b > 262143) ? 262143 : b);

                mRgbBuffer[bufferIndex++] = ((r << 6) & 0xff0000) |
                        ((g >> 2) & 0xff00) |
                        ((b >> 10) & 0xff);
            }
        }
    }

    public static byte[] imageToByteArray(Image image) {
        byte[] data = null;
        if (image.getFormat() == ImageFormat.JPEG) {
            Image.Plane[] planes = image.getPlanes();
            ByteBuffer buffer = planes[0].getBuffer();
            data = new byte[buffer.capacity()];
            buffer.get(data);
            return data;
        } else if (image.getFormat() == ImageFormat.YUV_420_888) {
            data = NV21toJPEG(
                    YUV_420_888toNV21(image),
                    image.getWidth(), image.getHeight());
        }
        return data;
    }

    public static byte[] YUV_420_888toNV21(Image image) {
        byte[] nv21;
        ByteBuffer yBuffer = image.getPlanes()[0].getBuffer();
        ByteBuffer uBuffer = image.getPlanes()[1].getBuffer();
        ByteBuffer vBuffer = image.getPlanes()[2].getBuffer();

        int ySize = yBuffer.remaining();
        int uSize = uBuffer.remaining();
        int vSize = vBuffer.remaining();

        nv21 = new byte[ySize + uSize + vSize];

        //U and V are swapped
        yBuffer.get(nv21, 0, ySize);
        vBuffer.get(nv21, ySize, vSize);
        uBuffer.get(nv21, ySize + vSize, uSize);

        return nv21;
    }

    public static byte[] NV21toJPEG(byte[] nv21, int width, int height) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        YuvImage yuv = new YuvImage(nv21, ImageFormat.NV21, width, height, null);
        yuv.compressToJpeg(new Rect(0, 0, width, height), 100, out);
        return out.toByteArray();
    }

//    private ByteBuffer convertYUV420ToN21(Image imgYUV420, boolean grayscale) {
//
//        Image.Plane yPlane = imgYUV420.getPlanes()[0];
//        byte[] yData = getRawCopy(yPlane.getBuffer());
//
//        Image.Plane uPlane = imgYUV420.getPlanes()[1];
//        byte[] uData = getRawCopy(uPlane.getBuffer());
//
//        Image.Plane vPlane = imgYUV420.getPlanes()[2];
//        byte[] vData = getRawCopy(vPlane.getBuffer());
//
//        // NV21 stores a full frame luma (y) and half frame chroma (u,v), so total size is
//        // size(y) + size(y) / 2 + size(y) / 2 = size(y) + size(y) / 2 * 2 = size(y) + size(y) = 2 * size(y)
//        int npix = imgYUV420.getWidth() * imgYUV420.getHeight();
//        byte[] nv21Image = new byte[npix * 2];
//        Arrays.fill(nv21Image, (byte)127); // 127 -> 0 chroma (luma will be overwritten in either case)
//
//        // Copy the Y-plane
//        ByteBuffer nv21Buffer = ByteBuffer.wrap(nv21Image);
//        for(int i = 0; i < imgYUV420.getHeight(); i++) {
//            nv21Buffer.put(yData, i * yPlane.getRowStride(), imgYUV420.getWidth());
//        }
//
//        // Copy the u and v planes interlaced
//        if(!grayscale) {
//            for (int row = 0; row < imgYUV420.getHeight() / 2; row++) {
//                for (int cnt = 0, upix = 0, vpix = 0; cnt < imgYUV420.getWidth() / 2; upix += uPlane.getPixelStride(), vpix += vPlane.getPixelStride(), cnt++) {
//                    nv21Buffer.put(uData[row * uPlane.getRowStride() + upix]);
//                    nv21Buffer.put(vData[row * vPlane.getRowStride() + vpix]);
//                }
//            }
//
//            fastReverse(nv21Image, npix, npix);
//        }
//
//        fastReverse(nv21Image, 0, npix);
//
//        return nv21Buffer;
//    }

    public static Bitmap convertToBitmap(Image image) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        ByteBuffer bufferY = image.getPlanes()[0].getBuffer();
        byte[] data0 = new byte[bufferY.remaining()];
        bufferY.get(data0);

        ByteBuffer bufferU = image.getPlanes()[1].getBuffer();
        byte[] data1 = new byte[bufferU.remaining()];
        bufferU.get(data1);

        ByteBuffer bufferV = image.getPlanes()[2].getBuffer();
        byte[] data2 = new byte[bufferV.remaining()];
        bufferV.get(data2);
        try {
            outputStream.write(data0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < bufferV.remaining(); i++) {
            outputStream.write(data1[i]);
            outputStream.write(data2[i]);
        }

        final YuvImage yuvImage = new YuvImage(outputStream.toByteArray(), ImageFormat.NV21, 1920,1080, null);
        ByteArrayOutputStream outBitmap = new ByteArrayOutputStream();

        yuvImage.compressToJpeg(new Rect(0, 0,1920, 1080), 95, outBitmap);

        byte[] imageBytes = outBitmap.toByteArray();

       return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
    }

}
