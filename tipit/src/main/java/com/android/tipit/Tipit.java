package com.android.tipit;

import android.graphics.Bitmap;
import android.graphics.Color;

/**
 * Created by Eugen on 23.10.2017.
 */

public class Tipit {

    public static void drawRectangleOnBitmap(Bitmap bitmap, int x, int y) {
        int x1 = x + 10;
        int y1 = y + 10;
        for (int i = x; i <= x1; i++) {
            for (int k = y; k <= y1; k++) {
                bitmap.setPixel(i, k, Color.rgb(255, 0, 0));
            }
        }
    }


}
